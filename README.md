Library of sort algorithms
--------------------------

- sort/Comb.java (file, implemented comb sort)
- sort/Bubble.java (file, implemented bubble sort)
- sort/Insert.java (file, implemented insert sort)
- sort/Quick.java (file, implemented quick sort)
- sort/CompInt.java (file, comparator for integers)
- CombTest.java (file, test comb sort)
- BubbleTest.java (file, test bubble sort)
- InsertTest.java (file, test insert sort)
- QuickTest.java (file, test insert sort)

Compile an example
------------------

- javac QuickTest.java

Run an example
--------------

- java QuickTest 7 55
- parameters (length of array to be sorted, maximum element in array)

Test environment
----------------

- javac version 14.0.2
- os lubuntu 16.04 lts
