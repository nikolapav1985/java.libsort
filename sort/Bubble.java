package sort;

import java.util.*;

/**
*
* class Bubble
*
* implement bubble sort
*
* number of steps to sort array n^2, n length of array
*
* count number of steps to sort an array
*
* (n-1)+(n-2)+(n-3)+...+1=(1/2)=(1/2)n(n+1)-n=(1/2)n(n-1)
*
* Big Oh n^2
*
*/

public class Bubble<T>{
    public void sort(int len,Comparator cmp,T[] arr){ // sort an array
        int sorted=0;
        T tmp;
        int i,j;
        
        for(;sorted==0;){ // do sorting
            sorted=1; // array could be sorted
            for(i=0,j=1;j<len;i++,j++){
                if(cmp.compare(arr[i],arr[j])>0){ // array not sorted
                    // arr[i] > arr[j] true
                    // need arr[i] < arr[j]
                    // do stuff...
                    // get arr[i] < arr[j]
                    sorted=0;
                    tmp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=tmp;
                }
            }
        }
    }
}
