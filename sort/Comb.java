package sort;

import java.util.*;

public class Comb<T>{
    public void sort(int len,Comparator cmp,T[] arr){ // sort an array
        int gap=len-1;
        int sorted=0;
        double div=1.2;
        T tmp;
        int i,j;
        
        for(;sorted==0;){ // do sorting
            if(gap>1){
                sorted=0;
            } else {
                gap=1;
                sorted=1;
            }
            for(i=0,j=gap;j<len;i++,j++){
                if(cmp.compare(arr[i],arr[j])>0){
                    sorted=0;
                    tmp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=tmp;
                }
            }
            gap=(int)(gap/div);
        }
    }
}
