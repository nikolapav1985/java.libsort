package sort;

import java.util.*;

public class CompInt implements Comparator{
    public int compare(Object a,Object b){
        int aa=(int)a;
        int bb=(int)b;
        if(aa==bb){
            return 0;
        }
        if(aa>bb){
            return 1;
        }
        return -1;
    }
}
