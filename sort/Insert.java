package sort;

import java.util.*;

/**
*
* class Insert
*
* implement insert sort
*
* number of steps to sort array n^2, n length of array
*
* count number of steps to sort an array
*
* (n-1)+(n-2)+(n-3)+...+1=(1/2)=(1/2)n(n+1)-n=(1/2)n(n-1)
*
* Big Oh n^2
*
*/

public class Insert<T>{
    public void sort(int gap,int len,Comparator cmp,T[] arr){ // sort an array
        int sorted=0;
        T tmp;
        int i,j;
        
        for(i=gap;i<=(len-gap);i+=gap){ // default gap = 1
            tmp=arr[i];
            for(j=i-gap;j>=0;j-=gap){
                if(cmp.compare(arr[j],tmp)>=1){
                    arr[j+gap]=arr[j];
                    continue;
                }
                break;
            }
            arr[j+gap]=tmp;
        }
    }
}
