package sort;

import java.util.*;

/**
*
* class Quick
*
* implement quick sort (sort array items around pivot)
*
*/

public class Quick<T>{
    public void sort(int lo,int hi,Comparator cmp,T[] arr){ // sort an array
        T tmp,pivot;
        int llo,hhi;

        if(lo>=hi)return; // stop recursion
        pivot=arr[(lo+hi)/2];
        llo=lo;
        hhi=hi;
        for(;llo<=hhi;){
            for(;cmp.compare(arr[llo],pivot)<0;){
                llo++;
            }
            for(;cmp.compare(arr[hhi],pivot)>0;){
                hhi--;
            }
            if(llo<=hhi){
                tmp=arr[llo];
                arr[llo]=arr[hhi];
                arr[hhi]=tmp;
                llo++;
                hhi--;
            }
        }
        this.sort(lo,hhi,cmp,arr);
        this.sort(llo,hi,cmp,arr);
    }
}
