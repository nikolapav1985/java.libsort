import sort.*;

class QuickTest{
    public static void main(String[] args){
        int len;
        int max;
        int i;
        Integer[] arr;
        Quick<Integer> quick=new Quick<Integer>();
        CompInt compInt=new CompInt();
        if(args.length != 2){
            System.exit(0);
        }
        len=Integer.parseInt(args[0]);
        max=Integer.parseInt(args[1]);
        arr=new Integer[len];
        for(i=0;i<len;i++){
            arr[i]=(int)(Math.random()*max);
        }
        for(i=0;i<len;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println(" ");
        quick.sort(0,len-1,compInt,arr);
        for(i=0;i<len;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println(" ");
    }
}
