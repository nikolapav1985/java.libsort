import sort.*;

class InsertTest{
    public static void main(String[] args){
        int len;
        int max;
        int i;
        int gap=1;
        Integer[] arr;
        Insert<Integer> insert=new Insert<Integer>();
        CompInt compInt=new CompInt();
        if(args.length != 2){
            System.exit(0);
        }
        len=Integer.parseInt(args[0]);
        max=Integer.parseInt(args[1]);
        arr=new Integer[len];
        for(i=0;i<len;i++){
            arr[i]=(int)(Math.random()*max);
        }
        for(i=0;i<len;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println(" ");
	    insert.sort(gap,len,compInt,arr);
        for(i=0;i<len;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println(" ");
    }
}
